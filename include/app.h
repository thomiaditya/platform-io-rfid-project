#pragma once
#ifndef MY_HEADER_H
#define MY_HEADER_H

#include <Arduino.h>
#include <esp32_button.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <WiFiManager.h>
#include <LiquidCrystal_I2C.h>
#include <MFRC522.h>
#include <HTTPClient.h>
#include <Preferences.h>
#include <esp_sleep.h>
#include <functional>

#define BUZZER_PIN 12
#define SS_PIN_MASUK 5
#define SS_PIN_KELUAR 15
#define RST_PIN 27
#define MODE_BUTTON_PIN 34
#define RESET_BUTTON_PIN 35 // not implemented yet, figuring out later
#define RX_PIN 16           // ESP32 RX pin
#define TX_PIN 17           // ESP32 TX pin
#define RELAY_PIN 14
#define REED_SENSOR_PIN 2
#define DOOR_WAITING_OPEN_PERIOD 30000

// SERVER NTP
#define GMT_OFFSET_SEC 6 * 3600
#define DAYLIGHTOFFSET_SEC 3600
#define IDLE_PERIOD 5000 // Idle period in milliseconds (5 seconds)
#define SLEEP_PERIOD 60000

typedef void (*RFIDCallback)(const String &);

extern Preferences preferences;
extern LiquidCrystal_I2C lcd;
extern MFRC522 rfidMasuk;
extern MFRC522 rfidKeluar;
extern CookieJar cookieJar;
extern String cookiesString;
extern HTTPClient http;

// Setting the state variables
extern String device_id;
extern String server_url;
extern String device_on_pass;
extern String OTP;

// States
extern bool writing_mode;
extern bool reset_wifi;
extern bool reset_wifi_confirm;
extern bool shouldSaveConfig;
extern String current_mode;
extern unsigned long lastIdleTime;            // Stores the last time an action was performed
extern bool welcomeShowed;

// Buttons
void push_button_long_press();
void double_clicked();
extern button_t mode_button;
extern button_t reset_button;

void IRAM_ATTR button_isr();

void printLCD(const String &str, int col, int row);
void printLCD(const String &str, int col);
void handleWriteRFID(const String &rfid);
void handleValidateRFID(const String &rfid);
void handleRFIDMasuk(const String &rfid);
void handleRFIDKeluar(const String &rfid);
void buzzBeep(unsigned int ms);
void buzzBeep(unsigned int beepCount, unsigned int beepDuration, unsigned int delayBetweenBeeps);
void configModeCallback(WiFiManager *myWiFiManager);
void readConfigurationFSJSON();
void handleWifiReset();
void saveConfigCallback();
String readKeypadInputSync(int stringLength, bool returnOnFull = true);
String readKeypadInputSync(int stringLength, bool returnOnFull, unsigned long timeout);
void sendRequest(const String &url, const JsonObject &jsonObject, std::function<void(const String &response)> onSuccess, const String &method = "POST");
void sendRequest(const String &url, const JsonObject &jsonObject, std::function<void(const String &response)> onSuccess, std::function<void(const String &response)> onFailure, const String &method);
void handleRFIDRead(MFRC522 &rfid, RFIDCallback callback);
void login();
void logout();
void controlLockActive(bool active);
bool openTheDoor();
void confirmCheckIn(bool checkedIn, const String &rfid);

// For helper functions
void initializeHardware();
void displayConnectionStatus();
void readConfigAndConnectToWiFi();
void saveConfig();
void printDeviceDetails();
void printWiFiDetails();
void initializeRFID();
void initializeButtons();
void setCookieJar();
void checkDevicePass();
void printWelcomeMessage();
void setup();
void checkModeChange();
void loop();
void changeWelcomeMessageStateToFalse();

#endif