#include "app.h"

void reinitializeRFIDReader()
{
    rfidMasuk.PCD_Reset();
    rfidMasuk.PCD_Init(); // Initialize MFRC522

    rfidKeluar.PCD_Reset();
    rfidKeluar.PCD_Init();
}

void controlLockActive(bool active)
{
    if (active)
    {
        digitalWrite(RELAY_PIN, HIGH);
    }
    else
    {
        digitalWrite(RELAY_PIN, LOW);
    }
}

void buzzBeep(unsigned int ms)
{
    digitalWrite(BUZZER_PIN, HIGH);
    delay(ms);
    digitalWrite(BUZZER_PIN, LOW);
}

void buzzBeep(unsigned int beepCount, unsigned int beepDuration, unsigned int delayBetweenBeeps)
{
    for (unsigned int i = 0; i < beepCount; i++)
    {
        digitalWrite(BUZZER_PIN, HIGH);
        delay(beepDuration);
        digitalWrite(BUZZER_PIN, LOW);

        // Avoid delay after the last beep
        if (i < beepCount - 1)
        {
            delay(delayBetweenBeeps);
        }
    }
}

bool openTheDoor()
{
    lcd.clear();
    buzzBeep(2, 100, 200);
    controlLockActive(false);
    printLCD("Door opened!", 0);

    unsigned long _time = millis();
    unsigned long _lastBeepTime = 0; // Variable to keep track of the last beep time

    // wait till the door opened
    while (digitalRead(REED_SENSOR_PIN) == LOW)
    {
        unsigned long _currentTime = millis();
        unsigned long _selisih = _currentTime - _time;

        // check if it's time to beep (every second during the last 3 seconds)
        if (_selisih >= DOOR_WAITING_OPEN_PERIOD - 3000 && _selisih < DOOR_WAITING_OPEN_PERIOD)
        {
            if (_currentTime - _lastBeepTime >= 1000)
            {                                 // check if a second has passed
                buzzBeep(100);                // beep for each second
                _lastBeepTime = _currentTime; // update the last beep time
            }
        }

        // exit the loop after 6 seconds
        if (_selisih >= DOOR_WAITING_OPEN_PERIOD)
        {
            buzzBeep(500);
            controlLockActive(true);
            reinitializeRFIDReader();
            return false;
        }
    }

    delay(5000);

    // wait till the door closed
    while (digitalRead(REED_SENSOR_PIN) == HIGH)
    {
    }

    delay(2000);

    buzzBeep(500);
    controlLockActive(true);
    reinitializeRFIDReader();
    return true;
}
