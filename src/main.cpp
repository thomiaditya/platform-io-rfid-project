#include <Arduino.h>
#include <time.h>
#include "app.h"

// Setting the state variables
String device_id;
String server_url;
String device_on_pass;
String OTP = "";

// States
bool writing_mode = false;
bool reset_wifi = false;
bool reset_wifi_confirm = false;
bool shouldSaveConfig = false;
String current_mode = writing_mode ? "WRITING MODE" : "READING MODE";
bool welcomeShowed = false;

unsigned long lastIdleTime = 0;            // Stores the last time an action was performed
unsigned long prevTime = 0;

void ESPSleep()
{
    lcd.noDisplay();
    lcd.noBacklight();
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_34, 1);
    esp_light_sleep_start();
    lcd.display();
    lcd.backlight();
}

void displayCurrentTime()
{
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo))
    {
        Serial.println("Failed to obtain time");
        return;
    }
    // char timeString[20];
    // strftime(timeString, sizeof(timeString), "%H:%M:%S", &timeinfo);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(&timeinfo, "%a, %d %b %y");
    lcd.setCursor(0, 1);
    lcd.print(&timeinfo, "%T");
}

void setup()
{
    // Initialize the NTP client
    configTime(GMT_OFFSET_SEC, DAYLIGHTOFFSET_SEC, "pool.ntp.org", "time.nist.gov", "time.google.com");

    initializeHardware();
    displayConnectionStatus();
    readConfigAndConnectToWiFi();
    saveConfig();
    printDeviceDetails();
    printWiFiDetails();
    initializeRFID();
    initializeButtons();
    setCookieJar();
    checkDevicePass();
    printWelcomeMessage();

    // cookieJar.begin()

    lastIdleTime = millis();
}

void loop()
{
    unsigned long currentTime = millis();

    // Check if the current time - last recorded time is greater than the idle period
    if (currentTime - lastIdleTime > IDLE_PERIOD)
    {
        if(currentTime - prevTime >= 1000) {
            // Display the current time
            displayCurrentTime();
            welcomeShowed = true;
            prevTime = currentTime;
        }
    }

    // if (currentTime - lastIdleTime > SLEEP_PERIOD)
    // {
    //     ESPSleep();
    //     lastIdleTime = currentTime;
    // }

    checkModeChange();
    handleWifiReset();
    handleRFIDRead(rfidMasuk, handleRFIDMasuk);
    handleRFIDRead(rfidKeluar, handleRFIDKeluar);
}