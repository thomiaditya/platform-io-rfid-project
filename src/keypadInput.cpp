#include "app.h"

String readKeypadInputSync(int stringLength, bool returnOnFull)
{
    while (Serial2.available() > 0)
    {
        Serial2.read();
    }
    String inputString = ""; // Initialize an empty string to store the input
    printLCD("_", 1);
    while (true)
    {
        if (Serial2.available() > 0)
        {
            char key = (char)Serial2.read(); // Read the incoming key
            if (key == 'D')
            {
                return inputString;
            }
            else if (key == 'B')
            {
                return "cancel";
            }
            else if (key == 'C')
            {
                if (inputString.length() > 0)
                {
                    inputString.remove(inputString.length() - 1);
                }
                // Clear the LCD display and print the updated input string
                printLCD(inputString + "_", 1);
                buzzBeep(50);
            }
            else
            {
                if (inputString.length() < stringLength)
                {
                    inputString += key;
                    printLCD(inputString + "_", 1);
                    buzzBeep(50);
                }

                if (inputString.length() >= stringLength)
                {
                    printLCD(inputString, 1);
                    if (returnOnFull)
                    {
                        delay(50);
                        return inputString;
                    }
                }
            }
        }
    }
}

String readKeypadInputSync(int stringLength, bool returnOnFull, unsigned long timeout)
{
    unsigned long startTime = millis(); // Record the start time
    while (Serial2.available() > 0)
    {
        Serial2.read();
    }
    String inputString = ""; // Initialize an empty string to store the input
    printLCD("_", 1);
    while (true)
    {
        // Check if the timeout has been reached
        if (millis() - startTime > timeout)
        {
            return "timeout";
        }

        if (Serial2.available() > 0)
        {
            char key = (char)Serial2.read(); // Read the incoming key
            if (key == 'D')
            {
                return inputString;
            }
            else if (key == 'B')
            {
                return "cancel";
            }
            else if (key == 'C')
            {
                if (inputString.length() > 0)
                {
                    inputString.remove(inputString.length() - 1);
                }
                // Clear the LCD display and print the updated input string
                printLCD(inputString + "_", 1);
                buzzBeep(50);
            }
            else
            {
                if (inputString.length() < stringLength)
                {
                    inputString += key;
                    printLCD(inputString + "_", 1);
                    buzzBeep(50);
                }

                if (inputString.length() >= stringLength)
                {
                    printLCD(inputString, 1);
                    if (returnOnFull)
                    {
                        delay(50);
                        return inputString;
                    }
                }
            }
        }
    }
}
