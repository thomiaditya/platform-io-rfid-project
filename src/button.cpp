#include "app.h"

button_t mode_button = {.long_press_fun = &push_button_long_press};
button_t reset_button = {
    .double_click_fun = &double_clicked,
    .long_press_fun = &push_button_long_press};

void IRAM_ATTR button_isr()
{
    button_update(&mode_button);
    button_update(&reset_button);
}

void configModeCallback(WiFiManager *myWiFiManager)
{
    lcd.clear();
    printLCD("Connection", 0);
    printLCD("config...", 1);
    Serial.println(WiFi.softAPIP());
    Serial.println(myWiFiManager->getConfigPortalSSID());
}

void double_clicked()
{
    if (reset_button.mode == DOUBLE_CLICKED)
    {
        if (!reset_wifi)
        {
            reset_wifi_confirm = true;
        }
        reset_button.mode = NONE;
    }
}

void push_button_long_press()
{
    if (mode_button.mode == LONG_PRESSED)
    {
        Serial.println("Long Pressed. Changing mode!");
        writing_mode = !writing_mode;
        mode_button.mode = NONE;
    }

    if (reset_button.mode == LONG_PRESSED)
    {
        reset_wifi = !reset_wifi;
        reset_wifi_confirm = false;
        reset_button.mode = NONE;
    }
}
