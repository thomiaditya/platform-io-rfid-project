#include "app.h"

Preferences preferences;

void readConfigurationFSJSON()
{
    // read configuration from FS json
    Serial.println("mounting preferences...");
    preferences.begin("app", false);
    device_id = preferences.getString("device_id", "default-device-name");
    server_url = preferences.getString("server_url", "http://192.168.1.4:8000");
    device_on_pass = preferences.getString("device_pass", "111111");
    // end read
}

void saveConfigCallback()
{
    shouldSaveConfig = true;
    // end save
}