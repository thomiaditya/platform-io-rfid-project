#include "app.h"

LiquidCrystal_I2C lcd(0x27, 16, 2);

void printLCD(const String &str, int col, int row)
{
    lcd.setCursor(0, col);
    lcd.print("                ");
    lcd.setCursor(row, col);
    lcd.print(str);
}

void printLCD(const String &str, int col)
{
    lcd.setCursor(0, col);
    lcd.print("                ");
    lcd.setCursor(0, col);
    lcd.print(str);
}
