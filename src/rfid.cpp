#include "app.h"

MFRC522 rfidMasuk(SS_PIN_MASUK, RST_PIN);
MFRC522 rfidKeluar(SS_PIN_KELUAR, RST_PIN);

void initializeRFID()
{
    SPI.begin();          // Initialize SPI bus
    rfidMasuk.PCD_Init(); // Initialize MFRC522
    rfidKeluar.PCD_Init();
    Serial.println("Tap an RFID/NFC tag on the RFID-RC522 reader");
}



void handleRFIDRead(MFRC522 &rfid, RFIDCallback callback)
{
    if (rfid.PICC_IsNewCardPresent())
    {
        if (rfid.PICC_ReadCardSerial())
        {
            // Play the buzzer
            buzzBeep(100);
            MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
            Serial.print("RFID/NFC Tag Type: ");
            Serial.println(rfid.PICC_GetTypeName(piccType));
            lcd.clear();
            printLCD("Loading...", 0);

            String uid = "";
            for (int i = 0; i < rfid.uid.size; i++)
            {
                uid += String(rfid.uid.uidByte[i], HEX);
            }
            Serial.println("UID: " + uid);

            rfid.PICC_HaltA();      // Halt PICC
            rfid.PCD_StopCrypto1(); // Stop encryption on PCD

            if (callback)
            {
                callback(uid);
            }
        }
    }
}

// ======================================================
// AUTHENTICATION
// ======================================================

int loginAttempts = 0;

void handleLoginSuccess(const String &response)
{
    // Handle login success
    Serial.println("Login successful");
    Serial.println(http.header("Set-Cookie"));
    delay(1000);
}

void handleLoginFailure(const String &response)
{
    // Handle login failure
    Serial.println("Login failed: " + response);
    printLCD("Failed!", 0);
    delay(300);

    // Increment login attempts counter
    loginAttempts++;

    // Retry login
    Serial.println("Retrying login...");
    login();
}

void login()
{
    // If maximum login attempts reached, stop retrying
    if (loginAttempts >= 3)
    {
        Serial.println("Maximum login attempts reached. Exiting...");
        ESP.restart();
        return;
    }

    // Create a JSON object for the login credentials
    JsonDocument loginData;
    loginData["email"] = "admin@example.com";
    loginData["password"] = "admin123";

    // Define the URL for the login endpoint
    String url = String(server_url) + "/api/admin/login";
    // Serial.println(url);

    lcd.clear();
    printLCD("Loading...", 0);

    // Send the login request using POST method
    sendRequest(url, loginData.as<JsonObject>(), handleLoginSuccess, handleLoginFailure, "POST");
}

void handleLogoutSuccess(const String &response)
{
    http.clearAllCookies();
}
void handleLogoutFailure(const String &response)
{
    // Handle logout failure
    Serial.println("Logout failed: " + response);
    printLCD("Logout failed", 0);
    printLCD("restarting...", 1);
    delay(1000);

    // Restart the ESP32
    ESP.restart();
}

void logout()
{
    // Define the URL for the logout endpoint
    String url = String(server_url) + "/api/admin/logout";

    lcd.clear();
    printLCD("Logging out...", 0);
    JsonDocument doc;
    doc["logout"] = "true";

    // Send the logout request using POST method
    sendRequest(url, doc.as<JsonObject>(), handleLogoutSuccess, handleLogoutFailure, "POST");
    cookiesString = "";
}

// ======================================================
// END AUTHENTICATION
// ======================================================

void handleWriteRFID(const String &rfid)
{
    lcd.clear();
    printLCD("Writing...", 0);

    String url = String(server_url) + "/api/rfid/create";
    printLCD("Enter id:", 0);
    String tagId = readKeypadInputSync(6, false);

    lcd.clear();
    printLCD("Loading...", 0);

    // Create a JSON object and add the RFID UID
    JsonDocument doc;
    doc["rfid_uid"] = rfid;
    doc["tag_id"] = tagId;

    sendRequest(
        url, doc.as<JsonObject>(), [](const String &response)
        {
        printLCD("Success sent!", 0);
        buzzBeep(2, 100, 50); },
        "POST");
}

String removeQuotes(String str)
{
    String result = ""; // Create an empty string to hold the result
    for (int i = 0; i < str.length(); i++)
    {
        if (str[i] != '\"')
        { // Check each character
            result += str[i];
        }
    }
    return result;
}

void confirmCheckIn(bool checkedIn, const String &rfid)
{
    if (checkedIn)
    {
        String url = String(server_url) + "/api/rfid/checkin/confirm";

        // Create a JSON object and add the RFID UID
        JsonDocument doc;
        doc["rfid_uid"] = rfid;

        sendRequest(
            url, doc.as<JsonObject>(), [](const String &response) {},
            "POST");
    }

    if (!checkedIn)
    {
        String url = String(server_url) + "/api/rfid/checkout/confirm";

        // Create a JSON object and add the RFID UID
        JsonDocument doc;
        doc["rfid_uid"] = rfid;

        sendRequest(
            url, doc.as<JsonObject>(), [](const String &response) {},
            "POST");
    }
    Serial.println("RFID confirmed: " + rfid);
}

bool onValidateSucess(const String &res)
{
    bool checkedIn = false;
    unsigned long timeout = 30000;
    OTP = removeQuotes(res);
    Serial.println(OTP);

    lcd.clear();
    printLCD("Enter OTP:", 0);
    String inputOTP = readKeypadInputSync(6, false, timeout);

    for (int i = 0; i < 3; i++)
    {
        if (inputOTP == "timeout")
        {
            lcd.clear();
            printLCD("Timeout", 0);
            delay(1000);
            break;
        }

        if (inputOTP == OTP)
        {
            checkedIn = openTheDoor();
            break;
        }

        lcd.clear();
        printLCD("Incorrect!", 0);
        delay(1000);
        printLCD("Enter OTP:", 0);
        inputOTP = readKeypadInputSync(6, false, timeout);
    }
    return checkedIn;
}

void handleValidateRFID(const String &rfid)
{
    String url = String(server_url) + "/api/rfid/validate";

    // Create a JSON object and add the RFID UID
    JsonDocument doc;
    doc["rfid_uid"] = rfid;

    sendRequest(
        url, doc.as<JsonObject>(), [&rfid](const String &response)
        {
        printLCD("Validated!", 0);
        bool checkedIn = onValidateSucess(response);
        if (checkedIn)
        {
            confirmCheckIn(true, rfid);
        }
        buzzBeep(2, 100, 50); },
        "POST");
}

void handleRFIDMasuk(const String &rfid)
{
    if (writing_mode)
    {
        handleWriteRFID(rfid);
        changeWelcomeMessageStateToFalse();
        return;
    }
    handleValidateRFID(rfid);
    changeWelcomeMessageStateToFalse();
}

void handleRFIDKeluar(const String &rfid)
{
    Serial.println("RFID Keluar scanned!");
    Serial.println(rfid);

    String url = String(server_url) + "/api/rfid/checkout";

    // Create a JSON object and add the RFID UID
    JsonDocument doc;
    doc["rfid_uid"] = rfid;

    sendRequest(
        url, doc.as<JsonObject>(), [&rfid](const String &response)
        {
            bool checkedOut = openTheDoor();
            if(checkedOut) {
                confirmCheckIn(false, rfid);
            }
            changeWelcomeMessageStateToFalse(); },
        "POST");
}