#include "app.h"

HTTPClient http;
CookieJar cookieJar;
String cookiesString = "";

bool generateCookieHeaderString()
{
    // TODO: Fix this
    if(cookiesString != "") return true;

    for (Cookie c : cookieJar)
    {
        Serial.println(c.name + ": " + c.value);
        // cookieString = i.name + "=" + i.value;
        if (cookiesString == "")
            cookiesString = c.name + "=" + c.value;
        else
            cookiesString += " ;" + c.name + "=" + c.value;
    }

    if(cookiesString != "") return true;
    else return false;
}

void handleWifiReset()
{
    WiFiManager wifiManager;
    if (reset_wifi && !reset_wifi_confirm)
    {
        buzzBeep(4, 200, 100);
        reset_wifi = false;
    }

    if (!reset_wifi && reset_wifi_confirm)
    {
        buzzBeep(1000);
        wifiManager.resetSettings();
        wifiManager.reboot();
    }
}

void sendRequest(const String &url, const JsonObject &jsonObject, std::function<void(const String &response)> onSuccess, const String &method)
{
    http.begin(url.c_str());
    http.addHeader("Content-Type", "application/json");

    if(generateCookieHeaderString()) {
        Serial.println(cookiesString);
        http.addHeader("Cookie", cookiesString);
    }

    if (method == "POST")
    {
        // Serialize JSON object to a string
        String postData;
        serializeJson(jsonObject, postData);

        int httpResponseCode = http.POST(postData);

        if (httpResponseCode == HTTP_CODE_OK)
        {
            const String &response = http.getString();
            onSuccess(response);
        }
        else
        {
            Serial.println("Error " + String(httpResponseCode) + ": " + (const String &)http.getString());
            printLCD("Error!", 0);
            changeWelcomeMessageStateToFalse();
        }
    }
    else if (method == "GET")
    {
        int httpResponseCode = http.GET();

        if (httpResponseCode == HTTP_CODE_OK)
        {
            const String &response = http.getString();
            onSuccess(response);
        }
        else
        {
            Serial.println("Error " + String(httpResponseCode) + ": " + (const String &)http.getString());
            printLCD("Error!", 0);
            changeWelcomeMessageStateToFalse();
        }
    }
    else
    {
        Serial.println("Invalid method: " + method);
    }

    http.end();
}

void sendRequest(const String &url, const JsonObject &jsonObject, std::function<void(const String &response)> onSuccess, std::function<void(const String &response)> onFailure, const String &method)
{
    http.begin(url.c_str());
    http.addHeader("Content-Type", "application/json");

    if(generateCookieHeaderString()) {
        http.addHeader("Cookie", cookiesString);
    }

    if (method == "POST")
    {
        // Serialize JSON object to a string
        String postData;
        serializeJson(jsonObject, postData);

        int httpResponseCode = http.POST(postData);

        if (httpResponseCode == HTTP_CODE_OK)
        {
            const String &response = http.getString();
            onSuccess(response);
        }
        else
        {
            const String &response = http.getString();
            onFailure(response);
        }
    }
    else if (method == "GET")
    {
        int httpResponseCode = http.GET();

        if (httpResponseCode == HTTP_CODE_OK)
        {
            const String &response = http.getString();
            onSuccess(response);
        }
        else
        {
            const String &response = http.getString();
            onFailure(response);
        }
    }
    else
    {
        Serial.println("Invalid method: " + method);
    }

    http.end();
}
