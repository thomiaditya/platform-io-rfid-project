#include "app.h"

static WiFiManager wifiManager;
static WiFiManagerParameter device_id_param("device_id", "Device id", "default-device-name", 40);
static WiFiManagerParameter server_url_param("server_url", "Server url", "http://192.168.1.4:8000", 64, "value=\"http://\"");
static WiFiManagerParameter device_passpin_param("device_pass", "Enter device passpin entered when device on!", "111111", 6);

void initializeHardware()
{
    WiFi.mode(WIFI_STA);
    Serial.begin(115200);
    Serial2.begin(115200, SERIAL_8N1, RX_PIN, TX_PIN);
    pinMode(BUZZER_PIN, OUTPUT);
    pinMode(RELAY_PIN, OUTPUT);
    controlLockActive(true);
    digitalWrite(BUZZER_PIN, LOW);

    pinMode(REED_SENSOR_PIN, INPUT_PULLUP); // Enable internal pull-up resistor

    lcd.init();
    lcd.backlight();
}

void displayConnectionStatus()
{
    printLCD("Waiting for", 0);
    printLCD("connection...", 1);
}

void readConfigAndConnectToWiFi()
{
    readConfigurationFSJSON();

    pinMode(RESET_BUTTON_PIN, INPUT_PULLDOWN);
    bool buttonReset = digitalRead(RESET_BUTTON_PIN);
    Serial.println(buttonReset);
    if(buttonReset == HIGH) {
        wifiManager.resetSettings();
    }

    wifiManager.addParameter(&device_id_param);
    wifiManager.addParameter(&server_url_param);
    wifiManager.addParameter(&device_passpin_param);
    wifiManager.setAPCallback(configModeCallback);
    wifiManager.setSaveConfigCallback(saveConfigCallback);
    if (!(wifiManager.autoConnect("ESP32-Access-Point", "password")))
    {
        Serial.println("Failed to connect to WiFi");
        return;
    }

    
}

void saveConfig()
{
    if (shouldSaveConfig)
    {
        Serial.println("Saving config...");
        device_id = device_id_param.getValue();
        server_url = server_url_param.getValue();
        device_on_pass = device_passpin_param.getValue();
        preferences.putString("device_id", device_id);
        preferences.putString("server_url", server_url);
        preferences.putString("device_pass", device_on_pass);
    }
    preferences.end();

    Serial.println("Device ID: " + device_id);
    Serial.println("Server URL: " + server_url);
    Serial.println("Pass" + device_on_pass);

    // Test connection
    String url = String(server_url);
    bool connected = false;

    lcd.clear();
    printLCD("Connecting...", 0);
    while (!connected)
    {
        http.begin(url);           // Start connection
        int httpCode = http.GET(); // Send the request

        if (httpCode > 0)
        { // Check the returning code
            Serial.printf("HTTP %d returned\n", httpCode);
            if (httpCode == HTTP_CODE_OK)
            {
                connected = true;
                Serial.println("Successfully connected to the server!");
            }
        }
        else
        {
            Serial.printf("Failed to connect, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end(); // End connection
        delay(200);
    }
}

void printDeviceDetails()
{
    Serial.println("Device ID: " + device_id);
    Serial.println("Server URL: " + server_url);
    Serial.println("Pass" + device_on_pass);
}

void printWiFiDetails()
{
    printLCD("Connected to", 0);
    printLCD(wifiManager.getWiFiSSID(), 1);
    Serial.println("Connected to WiFi");
}

void initializeButtons()
{
    button_add_default(&mode_button, MODE_BUTTON_PIN);
    button_add_default(&reset_button, RESET_BUTTON_PIN);
    button_init(&button_isr);
}

void setCookieJar()
{
    http.setCookieJar(&cookieJar);
    delay(1000);
}

void checkDevicePass()
{
    delay(1000);

    printLCD("Pin:", 0);
    while (readKeypadInputSync(6) != device_on_pass)
    {
        printLCD("Wrong!", 1);
        delay(500);
    }
    delay(100);
    buzzBeep(2, 100, 100);
}

void printWelcomeMessage()
{
    lcd.clear();
    printLCD("Welcome!", 0);
    delay(2000);
}

void checkModeChange()
{
    String new_mode = writing_mode ? "WRITING MODE" : "READING MODE";
    if (new_mode != current_mode)
    {
        buzzBeep(1, 500, 100);
        if (new_mode == "WRITING MODE")
        {
            printLCD("Pin:", 0);
            String userInput = "";
            while (true)
            {
                userInput = readKeypadInputSync(6, true, 30000);
                if (userInput == "timeout")
                    userInput = "cancel";

                if (userInput != device_on_pass && userInput != "cancel")
                {
                    printLCD("Wrong!", 1);
                    delay(500);
                }
                else
                    break;
            }
            
            if (userInput == "cancel" ) {
                writing_mode = false;
            }

            if (userInput != "cancel")
            {
                login();
                printLCD(new_mode, 0);
                current_mode = new_mode;
                changeWelcomeMessageStateToFalse();
            }

        }
        else
        {
            logout();
            lcd.clear();
            printLCD(new_mode, 0);
            current_mode = new_mode;
            changeWelcomeMessageStateToFalse();
        }
    }
}

void changeWelcomeMessageStateToFalse()
{
    welcomeShowed = false;
    lastIdleTime = millis();
}